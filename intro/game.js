var gameSize = createSize(800, 600);

var game = createGame(gameSize);

var rect1a = createRect(createPoint(0, 0), createSize(800, 10));
var rect1b = createRect(createPoint(0, 10), createSize(800, 10));
var rect1c = createRect(createPoint(0, 20), createSize(800, 10));
var rect2 = createRect(createPoint(20, 80), createSize(80, 80));
var rect3 = createRect(createPoint(80, 280), createSize(80, 80));

var bricks = [];

var guard = {
  rect: createRect(createPoint(40, 40), createSize(20, 20)),
  state: 0,
};

var timeStart = Date.now();

var lastTimeRect3WasClicked = null;

game.render = function (context) {
  rect1a.render(context, RED);
  rect1b.render(context, RED);
  rect1c.render(context, RED);
  rect2.render(context, BLUE);
  rect3.render(context, BLUE);

  bricks.forEach(function (brick) {
    var centerPoint = createPoint(brick.point.x - brick.size.width/2, brick.point.y - brick.size.height/2);
    var rect = createRect(centerPoint, brick.size);
    rect.render(context, ORANGE);
  });

  guard.rect.render(context, CYAN);
};

game.update = function (delta) {
  rect1a.size.width -= delta * 70;
  rect1b.size.width -= delta * 80;
  rect1c.size.width -= delta * 100;

  bricks.forEach(function (brick) {
    var age = Date.now() - brick.timestamp;
    brick.point.y += age * delta;
  });

  if (lastTimeRect3WasClicked) {
    var timeDiff = (Date.now() - lastTimeRect3WasClicked) / 1000;
    var change = timeDiff * 100;
    var newSize = createSize(80 + change, 80 + change);
    if (newSize.width < 200) {
      rect3.size = newSize;
    }
  }

  var guardSpeed = 500;
  if (guard.state == 0) { // down
    guard.rect.origin.y += guardSpeed * delta;
    if (guard.rect.origin.y + guard.rect.size.height + 40 > gameSize.height) {
      guard.state = 1;
    }
  } else if (guard.state == 1) { // right
    guard.rect.origin.x += guardSpeed * delta;
    if (guard.rect.origin.x + guard.rect.size.width + 40 > gameSize.width) {
      guard.state = 2;
    }
  } else if (guard.state == 2) { // up
    guard.rect.origin.y -= guardSpeed * delta;
    if (guard.rect.origin.y - 40 < 0) {
      guard.state = 3;
    }
  } else if (guard.state == 3) { // left
    guard.rect.origin.x -= guardSpeed * delta;
    if (guard.rect.origin.x - 40 < 0) {
      guard.state = 0;
    }
  }
};

game.onMouseDown = function (point) {
  if (rect1a.containsPoint(point)) {
    rect1a.size.width += 100;
  } else if (rect2.containsPoint(point)) {
    // only dragged
  } else if (rect3.containsPoint(point)) {
    lastTimeRect3WasClicked = Date.now();
  } else {
    var brick = {
      point: point,
      size: createSize(30, 10),
      timestamp: Date.now()
    };
    bricks.push(brick);
  }
};

game.onDrag = function(pointA, pointB) {
  if (rect2.containsPoint(pointA)) {
    rect2.origin.x += pointB.x - pointA.x;
    rect2.origin.y += pointB.y - pointA.y;
  }
};

game.run();
