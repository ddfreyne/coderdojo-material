var createGame = function (size) {
  var canvas = document.createElement("canvas");
  canvas.width  = size.width;
  canvas.height = size.height;
  document.body.appendChild(canvas);
  var context = canvas.getContext("2d");;

  var then = Date.now();
  var game = {
    render: function(context) {},
    update: function(delta) {},
    onMouseDown: function(point) {},
    onMouseUp: function(point) {},
    onDrag: function(pointA, pointB) {},
    run: function() { then = Date.now(); step(); }
  }

  function step() {
    window.requestAnimationFrame(step);

    var now = Date.now();
    var delta = (now - then) / 1000;

    context.clearRect(0, 0, size.width, size.height);
    game.render(context);
    game.update(delta);

    then = now;
  }

  function eventPositionCanvasCoordinates(event) {
    var canvasOffset = $(canvas).offset();
    return {
      x: event.pageX - canvasOffset.left,
      y: event.pageY - canvasOffset.top,
    };
  }

  var lastDownPos = null;

  function mouseDownHandler(event) {
    var pos = eventPositionCanvasCoordinates(event);
    game.onMouseDown(createPoint(pos.x, pos.y));
    lastDownPos = pos;
  };

  function mouseUpHandler(event) {
    var pos = eventPositionCanvasCoordinates(event);
    game.onMouseUp(createPoint(pos.x, pos.y));
    lastDownPos = null;
  };

  function mouseMoveHandler(event) {
    if (lastDownPos) {
      var pos = eventPositionCanvasCoordinates(event);
      game.onDrag(lastDownPos, pos);
      lastDownPos = pos;
    }
  }

  canvas.addEventListener("mousedown", mouseDownHandler, false);
  canvas.addEventListener("mouseup", mouseUpHandler, false);
  canvas.addEventListener("mousemove", mouseMoveHandler, false);

  return game;
}

var createPoint = function (x, y) {
  return {
    x: x,
    y: y,
  }
}

var createSize = function (width, height) {
  return {
    width: width,
    height: height,
  }
}

var _RectProto = {
  render: function (context, style) {
    context.save();
    context.fillStyle = style;
    context.fillRect(this.origin.x, this.origin.y, this.size.width, this.size.height);
  },

  containsPoint: function (point) {
    return this.left() <= point.x && point.x <= this.right() &&
           this.top()  <= point.y && point.y <= this.bottom();
  },

  top: function () {
    return this.origin.y;
  },

  bottom: function () {
    return this.origin.y + this.size.height;
  },

  left: function () {
    return this.origin.x;
  },

  right: function () {
    return this.origin.x + this.size.width;
  },

  xCenter: function () {
    return this.origin.x + this.size.width/2;
  },

  yCenter: function () {
    return this.origin.y + this.size.height/2;
  },
}

createRect = function (origin, size) {
  var rect = Object.create(_RectProto);
  rect.origin = origin;
  rect.size = size;
  return rect;
}

var RED    = "rgb(255, 0, 0)";
var ORANGE = "rgb(255, 192, 0)";
var BLUE   = "rgb(0, 0, 255)";
var CYAN   = "rgb(0, 255, 255)";
